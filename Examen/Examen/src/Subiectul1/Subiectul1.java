package Subiectul1;

public class Subiectul1 {
    interface Y{
        int f();
    }
    public class I implements Y {
        private long t;
        public int f(){
            return 0;
        }
    }
    public class K {
        L p;
        K(){
            p = new L();
        }
    }
    public class J {
        public void b(){
            I i;
        }
    }
    public class L {
        public void metA(){

        }
    }
    public class M {
        public void metB(){

        }
    }
    public class N{

    }

}
