package Subiectul2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 {
    private JPanel panel;
    private JTextField textField1;
    private JTextField textField2;
    private JButton button1;

    public S2() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String string = textField1.getText();
                textField2.setText(string.toString());

            }
        });

    }

    public static void main (String[]args){
        JFrame frame = new JFrame("Subiectul2");
        frame.setContentPane(new S2().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(250, 250);

    }
}
